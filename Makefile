#!/usr/bin/make -f

DOCKER_COMPOSE_DEV = docker-compose

#----DEV
.PHONY: setup
setup:
	${DOCKER_COMPOSE_DEV} pull

.PHONY: start
start:
	${DOCKER_COMPOSE_DEV} up -d

.PHONY: install
install:
	${DOCKER_COMPOSE_DEV} run --rm cli sh -c "ssh-agent -s && composer install --no-interaction"
	make prod_cache

.PHONY: prod_cache
prod_cache:
	${DOCKER_COMPOSE_DEV} run --rm cli bin/console  cache:clear --env=prod

.PHONY: stop
stop:
	${DOCKER_COMPOSE_DEV} stop

.PHONY: remove
remove:
	${DOCKER_COMPOSE_DEV} rm -fv

.PHONY: clean-project
clean-project:
	${DOCKER_COMPOSE_DEV} down -v --rmi local --remove-orphans
	rm -rf var/cache/* vendor var/logs/*